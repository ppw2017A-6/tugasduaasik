from django.shortcuts import render
from custom_auth.models import User

# Create your views here.
def index(request, company_id):
    profile = User.objects.filter(company_id=company_id).first()
    html = 'company_profile/company_profile.html'
    response = {
        'profile':profile,
        'login':request.user.is_authenticated,
    }
    return render(request, html, response)