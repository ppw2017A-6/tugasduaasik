from django.conf.urls import url
from .views import index

urlpatterns = [
    url(r'^(?P<company_id>\d+)/$', index, name='index'),
]