from django.test import TestCase
from django.test import Client
from django.urls import reverse
from custom_auth.models import User
from forum.models import Forum
from .models import Comment


# # Create your tests here.
# def create_user():
# 	return User.objects.create(company_id="1234", username="1234")


# class CommentsUnitTest(object):
# 	"""docstring for CommentsUnitTest"""
# 	def test_function_index(request):#pragma: no cover
# 		user = create_user()
# 		forum = Forum.objects.create(forum="coba yaa", user=user)
# 		Comment.objects.create(comment="cobain aja dulu", user=user, forum=forum)

# 		response_get = Client().get(reverse("comments:index"))
# 		html_response = response_get.content.decode('utf8')
# 		self.assertEqual(response_get.status_code, 200)
# 		self.assertIn("coba yaa", html_response)
# 		self.assertIn("cobain aja dulu", html_response)

# 	def test_add_comment(request):#pragma: no cover
# 		user = create_user()
# 		forum = Forum.objects.create(forum="coba yaa", user=user)
# 		Comment.objects.create(comment="cobain aja dulu", user=user)

# 		response_post = Client().post(reverse("comments:add-comment"),{"comment": "Norman ganteng", "forum":forum})
# 		self.assertEqual(response_post.status_code, 302)
# 		self.assertEqual(Comment.objects.all().count(),1)
# 		self.assertEqual(Comment.objects.first().comment, "Norman ganteng")

# 	def test_post_comment_error(self):#pragma: no cover
#         user = create_user()
#         forum = Forum.objects.create(forum="coba yaa", user=user)
#         response_post = Client().post(reverse("comments:add-comment"),{"comment": "", "forum":forum})
#         self.assertEqual(response_post.status_code, 302)
#         self.assertEqual(Status.objects.all().count(),0)


