from custom_auth.models import User


class CompanyBackend:

    def authenticate(self, request, company_id=None, *args, **kwargs):
        user, created = User.objects.get_or_create(
            company_id=company_id,
            username=company_id,
        )
        user.name = kwargs.get('name')
        user.photo_url = kwargs.get('photo_url')
        user.profile_url = kwargs.get('profile_url')
        user.company_type = kwargs.get('company_type')
        user.specialty = kwargs.get('specialty')
        user.save()
        return user

    def get_user(self, user_id):
        return User.objects.filter(pk=user_id).first()
