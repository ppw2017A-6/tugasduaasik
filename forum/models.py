# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from custom_auth.models import User
from django.conf import settings

# Create your models here.
class Forum(models.Model):
    forum = models.TextField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='forums', on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
 
    class Meta:
        ordering = ['-date_created']