from django.conf.urls import url
from .views import login_index, logout_index, custom_login, custom_logout


urlpatterns = [
    url(r'^login_page/$', login_index, name='login_index'),
    url(r'^logout_page/$', logout_index, name='logout_index'),
    url(r'^login/$', custom_login, name='custom_login'),
    url(r'^logout/$', custom_logout, name='custom_logout')
]
