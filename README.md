Site: http://tugasduaasik.herokuapp.com

# Coverage And Pipeline

[![coverage report](https://gitlab.com/ppw2017A-6/tugasduaasik/badges/master/coverage.svg)](https://gitlab.com/ppw2017A-6/tugasduaasik/commits/master)

[![pipeline status](https://gitlab.com/ppw2017A-6/tugasduaasik/badges/master/pipeline.svg)](https://gitlab.com/ppw2017A-6/tugasduaasik/commits/master)

# Contributor

1. Athifah Fidelia Sectianri - 1606874684
2. Nabil Tegar - 1606874596
3. Norman Bintang - 1606862772
4. Rahmania Astrid Mochtar - 1606828702