from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser


# Create your models here.
class User(AbstractUser):
    company_id = models.CharField(max_length=100)
    name = models.CharField(max_length=100, blank=True, null=True)
    company_type = models.CharField(max_length=150, blank=True, null=True)
    profile_url = models.URLField(default="http://google.com", blank=True, null=True)
    photo_url = models.URLField(default="https://cdn2.iconfinder.com/data/icons/large-home-icons/512/Company_factory_group_office_building_people.png", blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    specialty = models.CharField(max_length=150, blank=True, null=True)
