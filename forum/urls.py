from django.conf.urls import url
from .views import index, add_forum, add_comment


urlpatterns = [
    url(r'^$', index, name='forum-index'),
    url(r'^add-forum/', add_forum, name='add-forum'),
    url(r'^add-comment/', add_comment, name='forum-add-comment'),
]