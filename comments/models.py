# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from custom_auth.models import User
from django.conf import settings
from forum.models import Forum

# Create your models here.
class Comment(models.Model):
    comment = models.TextField(max_length=1000)
    forum = models.ForeignKey(Forum, related_name='comments', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='comments', on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
 
    class Meta:
        ordering = ['date_created']