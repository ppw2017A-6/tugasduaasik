from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from forum.models import Forum
from .models import Comment
from .forms import Comment_Form

# Create your views here.
response = {}

def index(request):
    html = 'comments/comments.html'
    forum_list = Forum.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(forum_list, 6)
    try:
        forums = paginator.page(page)
    except PageNotAnInteger:
        forums = paginator.page(1)
    except EmptyPage:
        forums = paginator.page(paginator.num_pages)
    forum_with_comments = [{'forum':forum, 'comments':forum.comments.all()} for forum in forums]
    response['forums'] = forums
    response['forum_with_comments'] = forum_with_comments
    response['login'] = request.user.is_authenticated
    return render(request, html, response)


def add_comment(request):
    form = Comment_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        user = request.user if request.user.is_authenticated else None
        response['comment'] = request.POST['comment']
        comment = Comment(comment=response['comment'],forum=Forum.objects.get(id=request.POST['forumID']),user=user)
        comment.save()
        messages.info(request, 'Your comment has been posted successfully!')
        return HttpResponseRedirect('/')
    else:
        print(form.errors)
        return HttpResponseRedirect('/')
