from django.urls import resolve
from django.test import TestCase
from django.test import Client
from .models import User
from .views import login_index, logout_index, custom_login, custom_logout


# def create_user():
#     return User.objects.create(name="Nabil", username="123", company_id="123")


class FriendUnitTest(TestCase):

    def test_get_login_index(self):
        response = self.client.get('/custom_auth/login_page/')
        self.assertEqual(response.status_code, 200)

    def test_get_logout_index(self):
        response = self.client.get('/custom_auth/logout_page/')
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = self.client.post('/custom_auth/login/', {'company_id': '123123'})
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        self.client.post('/custom_auth/login/', {'company_id': '123123'})
        response = self.client.post('/custom_auth/logout/')
        self.assertEqual(response.status_code, 200)
