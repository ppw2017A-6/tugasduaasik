from django.http import JsonResponse
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user
from django.views.decorators.csrf import csrf_exempt


response = {}


def login_index(request):
    html = 'custom_auth/custom_auth_login.html'
    response['login'] = request.user.is_authenticated
    return render(request, html, response)


def logout_index(request):
    html = 'custom_auth/custom_auth_logout.html'
    response['login'] = request.user.is_authenticated
    return render(request, html, response)


@csrf_exempt
def custom_login(request):
    company_id = request.POST.get('company_id')
    name = request.POST.get('name')
    specialty = request.POST.get('specialty')
    photo_url = request.POST.get('photo_url')
    profile_url = request.POST.get('profile_url')
    company_type = request.POST.get('company_type')

    user = authenticate(
        company_id=company_id,
        name=name,
        specialty=specialty,
        photo_url=photo_url,
        profile_url=profile_url,
        company_type=company_type,
    )

    if user is not None:
        login(request, user)

    return JsonResponse({'message': 'login success'})


@csrf_exempt
def custom_logout(request):
    logout(request)
    return JsonResponse({'message': 'logout success'})
