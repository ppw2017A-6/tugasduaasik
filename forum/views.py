# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from custom_auth.models import User
from .models import Forum
from .forms import Forum_Form
from comments.forms import Comment_Form
from comments.models import Comment 
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required

# Create your views here.
response = {}

@login_required(login_url="/custom_auth/login_page/")
def index(request):
    html = 'forum/forum.html'
    forum_list = request.user.forums.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(forum_list, 6)
    try:
        forums = paginator.page(page)
    except PageNotAnInteger:
        forums = paginator.page(1)
    except EmptyPage:
        forums = paginator.page(paginator.num_pages)
    forum_with_comments = [{'forum':forum, 'comments':forum.comments.all()} for forum in forums]
    response['forums'] = forums
    response['forum_with_comments'] = forum_with_comments
    response['login'] = request.user.is_authenticated
    return render(request, html, response)

@login_required(login_url="/custom_auth/login_page/")
def add_forum(request):
    form = Forum_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['forum'] = request.POST['forum']
        forum = Forum(forum=response['forum'],user=request.user)
        forum.save()
        messages.info(request, 'Your forum has been posted successfully!')
        return HttpResponseRedirect('/forum/')
    else:
        print(form.errors)
        return HttpResponseRedirect('/forum/')

@login_required(login_url="/custom_auth/login_page/")
def add_comment(request):
	form = Comment_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['comment'] = request.POST['comment']
		comment = Comment(comment=response['comment'],forum=Forum.objects.get(id=request.POST['forumID']),user=request.user)
		comment.save()
		messages.info(request, 'Your comment has been posted successfully!')
		return HttpResponseRedirect('/forum/')
	else:
		print(form.errors)
		return HttpResponseRedirect('/forum/')