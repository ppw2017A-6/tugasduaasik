from django.test import TestCase
from django.test import Client
from django.urls import resolve
from company_profile.views import index
from custom_auth.models import User

# Create your tests here.
def create_user():
	return User.objects.create(company_id="12345",username="nabiil", name="nabil tegar")

class CompanyProfileUnitTest(TestCase):

	def test_get_profile_success(self):
		user = create_user()
		response_get = Client().get("/company/12345/")
		html_response = response_get.content.decode('utf8')
		self.assertIn(user.name, html_response)

	def test_use_index_function(self):
		user = create_user()
		response = resolve("/company/12345/")
		self.assertEqual(response.func, index)

