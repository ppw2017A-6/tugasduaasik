from django.forms import ModelForm
from forum.models import Forum

class Forum_Form(ModelForm):
    class Meta:
        model = Forum
        fields = ['forum']